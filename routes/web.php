<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@homePage');
Route::post('/', 'PagesController@loadPostsAjax');
Route::get('/post/{slug}', 'PagesController@singlePost')->name('single.post');
Route::get('/category/{slug}', 'PagesController@category')->name('category.list');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

// Vue
Route::get('/vue', function (){
    return view('vue');
});

Route::get('/vue/{any}', function (){
    return view('vue');
})->where('any', '.*');
