<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vue</title>

    <script src="https://kit.fontawesome.com/2db8864c20.js" crossorigin="anonymous"></script>
    <script>
        var Routes = {
            home: "{{ url('/') }}"
        };
    </script>
</head>

<body>
    <div id="app">
        <router-view></router-view>
        <hr>
        <router-link :to="{ name: 'home' }">Home</router-link>
        <router-link :to="{ name: 'about' }">About</router-link>
    </div>
{{ HTML::script('js/app.js') }}
</body>
</html>
