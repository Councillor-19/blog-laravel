<span>Follow</span>
<ul>
    @foreach($items as $menu_item)
        <li>
            <a
                href="{{ $menu_item->url }}"
                target="{{ $menu_item->target }}"
            >
                <i class="{{ $menu_item->icon_class }}"></i>
                <span>{{ $menu_item->title }}</span>
            </a>
        </li>
    @endforeach
</ul>
