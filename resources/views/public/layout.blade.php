<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ setting('site.title') }}</title>
    {{ HTML::style('css/app.css') }}
    <script src="https://kit.fontawesome.com/2db8864c20.js" crossorigin="anonymous"></script>
    <script>
        var Routes = {
            home: "{{ url('/') }}"
        };
    </script>
</head>

<body>
<header class="site-header">
    <div class="container container-xl">
        <div class="row">
            <div class="site-branding">
                <a href="/">
                    <img src="{{ '/storage/' . setting('site.logo') }}" alt="{{ setting('site.title') }}" title="{{ setting('site.title') }}">
                </a>
            </div>
            <nav class="navbar">
                {{ menu('Main menu') }}
            </nav>
        </div>
    </div>
</header>


@yield('content')

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="left-menu">
                {{ menu('Footer menu') }}
            </div>
            <div class="right-menu">
                {{ menu('Social Links', 'my_menu') }}
            </div>
        </div>
    </div>
</footer>
{{ HTML::script('js/app.js') }}
</body>
</html>
