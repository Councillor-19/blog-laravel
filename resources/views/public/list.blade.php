@php
    /**
    @var App\Post $post
    @var App\Category $category
    */
@endphp

@extends('public.layout')

@section('content')

    <div class="container container-xl">
       <h1 class="page-title">{{ $category->name }}</h1>
    </div>

    @if($posts)
        <div class="container">
            <div class="posts-wrapper">
                <div class="row row-listing" id="load-data">
                    @foreach($posts as $post)
                        @include('partials.post-loop-half')
                    @endforeach
                </div>
                {{ $posts->links() }}
            </div>
        </div>
    @endif

@endsection

