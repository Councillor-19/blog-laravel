@extends('public.layout')

@section('content')

    <div class="container container-xl">
        <div class="main-image">
            <img src="{{ asset('images/image.png') }}" alt="" class="img-responsive img-width">
        </div>
    </div>

    @if($posts)
        <div class="container">
            <div class="posts-wrapper">
                <div class="row row-listing">

                    @for($i = 1; $i < 5; $i++)
                        @if(!empty($posts[$i]))
			                <?php $post = $posts[$i];?>
                            @switch($i)
                                @case(1)
                                    @include('partials.post-loop-horisontal')
                                @default
                                   @include('partials.post-loop-half')
                            @endswitch
                        @endif
                    @endfor

                </div>
            </div>
        </div>
    @endif

    <section class="section section-sign-up">
        <h2 class="section-title">Sign up for our newsletter!</h2>
        <form action="" class="sign-up-form">
            <label for="sign-up-email">
                <input id="sign-up-email" type="text" placeholder="Enter a valid email address">
            </label>
            <input type="submit" value="submit">
        </form>
    </section>

    @if($posts && count($posts) > 5)
        <div class="container">
            <div class="posts-wrapper">
                <div class="row row-listing" id="load-data">
                    @for($i = 5; $i < count($posts); $i++)
		                <?php $post = $posts[$i];?>
                        @include('partials.post-loop-half')
                    @endfor
                </div>
                <div class="btn-wrapper">
                    <a href="#" id="btn-more" data-id="{{$posts[count($posts) - 1]->id}}" data-token="{{ csrf_token() }}" class="btn btn-gray">Load more</a>
                </div>
            </div>
        </div>
    @endif

@endsection

