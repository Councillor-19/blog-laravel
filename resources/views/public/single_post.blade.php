<?php /** @var App\Post $post */ ?>

@extends('public.layout')

@section('content')

    <article class="single-post">

        <div class="container container-xl">
            <div class="main-image">
                <img src="{{ Voyager::image( $post->image ) }}" alt="{{ $post->title }}" title="{{ $post->title }}" class="img-responsive img-width">
            </div>
        </div>

        <div class="container">

            <div class="post">

                <div class="post-category">
                    {{ $post->category->name }}
                </div>
                <header>
                    <h1 class="post-title">{{ $post->title }}</h1>
                </header>
                <div class="post-content html">
                    {!! $post->body !!}
                </div>

            </div>

        </div>

    </article>

    <section class="section section-resent">
        <div class="container">
            <h2 class="section-title">YOU MAY ALSO LIKE</h2>

            <div class="row">
                @foreach($post->related() as $related)
                    <article class="related-post">
                        <div class="image-wrapper">
                            <a href="{{ route('single.post', $related->slug) }}">
                                <img src="{{ Voyager::image( $related->thumbnail('cropped') ) }}" alt="{{ $related->title }}" title="{{ $related->title }}" class="img-responsive img-width">
                            </a>
                        </div>
                        <div class="related-title">
                            <a href="{{ route('single.post', $related->slug) }}">
                                {{ $related->title }}
                            </a>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>

    </section>
    @if($post->hasComments())
        <div class="section-comments">
            <div class="container">

                <div class="comments-count">
                    {{$post->commentsCount()}}
                </div>
                <div class="comments-listing">
                    @foreach($post->comments as $comment)
                        @include('partials.comment-loop')
                    @endforeach

                    <div class="comment-item">
                        <div class="avatar">
                            <img src="{{ asset('images/no-photo.jpg') }}" alt="avatar" class="img-responsive img-width">
                        </div>
                        <div class="comment-content">
                            <form action="" class="comment-form">
                                <label for="comment" class="screen-reader-text">JOIN THE DISCUSSION</label>
                                <input type="text" name="comment" id="comment" placeholder="JOIN THE DISCUSSION">
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif


@endsection

