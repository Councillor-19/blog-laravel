<?php /** @var App\Comment $comment */ ?>

<div class="comment-item">
    <div class="avatar">
        <img src="{{ Voyager::image( $comment->author->avatar ) }}" alt="avatar" class="img-responsive img-width">
    </div>
    <div class="comment-content">
        <div class="author-name">{{ $comment->author->name }}</div>
        <div class="comment-content">
            {{ $comment->text }}
        </div>
        <a href="#" class="btn btn-text">REPLY</a>
    </div>
</div>
