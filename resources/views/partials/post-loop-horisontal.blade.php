<?php /** @var App\Post $post */ ?>

<article class="post post-horizontal">
    <div class="post-category">
        {{ $post->category->name }}
    </div>
    <header class="post-title">
        <a href="{{ route('single.post', $post->slug) }}">{{ $post->title }}</a>
    </header>
    <div class="post-excerpt">
        {{ $post->getExcerpt() }}
    </div>
    <a href="#" class="btn btn-text">Leave a comment</a>
</article>
