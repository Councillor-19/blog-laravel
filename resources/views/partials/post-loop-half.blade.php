<?php /** @var App\Post $post */ ?>

<article class="post post-half">

    <div class="post-image">
        <a href="{{ route('single.post', $post->slug) }}">
            <img src="{{ Voyager::image( $post->thumbnail('cropped') ) }}" alt="{{ $post->title }}" title="{{ $post->title }}" class="img-responsive img-width">
        </a>
    </div>
    <div class="post-category">
        <a href="{{ route('category.list', $post->category->slug) }}">
            {{ $post->category->name }}
        </a>
    </div>
    <header class="post-title">
        <a href="{{ route('single.post', $post->slug) }}">{{ $post->title }}</a>
    </header>
    <div class="post-excerpt">
        {{ $post->getExcerpt() }}
    </div>

</article>
