import Home from "./components/Home";
import NotFound from "./components/NotFound";

let About = () => import("./components/About");

export default {
    mode: 'history',
    linkActiveClass: 'current-page',

    routes: [
        {
            path: '*',
            component: NotFound,
        },
        {
            path: '/vue',
            component: Home,
            name: 'home'
        },
        {
            path: '/vue/about',
            component: About,
            name: 'about'
        },
    ]
}
