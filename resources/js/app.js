require('./bootstrap');
import $ from 'jquery';
import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes'

Vue.use(VueRouter);
window.$ = window.jQuery = $;

$(document).ready(function(){

    $(document).on('click','#btn-more',function(e) {
        e.preventDefault();
        var id = $(this).data('id'),
            token = $(this).data('token');
        console.log(id);
        //$("#btn-more").html("Loading....");
        $.ajax({
            url : Routes.home,
            method : "POST",
            data : {id:id, _token:token},
            dataType : "text",
            success : function (data)
            {
                if(data != '') {
                    $('#load-data').append(data);
                    $('#btn-more').attr('data-id', Number($('#btn-more').data('id')) + 2).data('id', Number($('#btn-more').data('id')) + 2);
                } else {
                    $('#btn-more').remove();
                }
            }
        });
    });

});


const app_ex = new Vue({
    el: '#app',

    router: new VueRouter(routes)
});

