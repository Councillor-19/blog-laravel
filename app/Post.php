<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends \TCG\Voyager\Models\Post
{

    public function comments()
    {
        return $this->hasMany('App\Comment')
            ->where('status', '<>', 0);
    }

    public function getExcerpt()
    {
        if(!empty($this->excerpt))
            return $this->excerpt;

        if(!empty($this->body)) {
            return mb_substr(strip_tags($this->body), 0, 255);
        }
        return false;
    }

    public function getCategoryID()
    {
        return (!empty($this->category))
            ? $this->category->id
            : null;
    }

    public function related()
    {
        return self::where('category_id', $this->getCategoryID())->where('id', '<>', $this->id)->limit(3)->get();
    }

    public function hasComments()
    {
        return ( count($this->comments) > 0 ) ? true : false;
    }

    public function commentsCount()
    {
        $count = count($this->comments);
        switch ($count){
            case 0:
                $text = "No Comments";
                break;
            case 1:
                $text = "1 Comment";
                break;
            default:
                $text = $count . " Comments";
                break;
        }
        return $text;
    }
}
