<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PagesController extends Controller
{
    public function homePage(Request $request)
    {

        if ($request->ajax()) {
            $posts = Post::orderBy('created_at','DESC')->limit(7)->get();
            $view = view('data',compact('posts'))->render();
            return response()->json(['html'=>$view]);
        } else {
            $posts = Post::paginate(7);
            return view('public.home', compact('posts'));
        }

    }

    public function loadPostsAjax(Request $request)
    {
        $output = '';
        $id = $request->id;

        $posts = Post::where('id','>',$id)->orderBy('created_at','DESC')->limit(2)->get();
        if(!$posts->isEmpty()) {
            foreach($posts as $post) {
                $output .= view('partials.post-loop-half', compact('post'))->render();
            }
        }
        echo $output;
    }

    public function singlePost($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        return view('public.single_post', compact('post'));
    }

    public function category($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $posts = $category->posts()->paginate(2);

        return view('public.list', compact('posts', 'category'));
    }

}
